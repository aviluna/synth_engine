#pragma once

#include <stack>
#include <list>

struct AudioContext;

#include "sample.hpp"
#include "audiogenerator.hpp"

/* * * * * * * * * * * * * *
 * Aviluna Research notes: *
 * * * * * * * * * * * * * *
 *
 * AUDIO BUFFER CREATION POLICY:
 *  Allocated by:
 *    AudioGenerators
 *
 *  Derived classes should NOT modify the buffer creation code.
 *
 *  AUDIO BUFFER MEMORY SAFETY POLICY:
 *
 *  Write to:
 *    AudioGenerators
 *
 *  Read/write to:
 *    AudioProcessors (via AudioSink(s) residing inside AudioProcessor)
 *
 *  Read/recycle to:
 *    AudioSink
 *
 *
 *  On allocation, AudioGenerators should first attempt to pop a pre-allocated,
 * cache-hot buffer from the BufferRecycler.
 *  On deallocation, AudioSink should push the buffer onto the BufferRecycler.
 *
 *  AudioDevice may free (REAL DEALLOCATE) buffers from the BufferRecycler when
 *  conditions change.
 *
*/

typedef std::stack<Sample *> AudioBufferRecycler;
typedef std::list<AudioGenerator *> AudioGeneratorList;

struct AudioContext{
  unsigned            buffer_size
  // Set by AudioDevice; in Sample units, not bytes
  ;
  AudioBufferRecycler buffer_recycler
  // Used for allocations / deallocations
  ;

  //

  void                recycle(Sample *)
  // Used for de-allocating memory in AudioSink.
    ;

  Sample *            recycle_alloc()
  // Used for the first attempt at allocating memory in AudioGenerator.
     ;

  void                recycler_invalidate(); // Called only by AudioDevice

  //

  AudioGeneratorList  generator_list;

  //

  unsigned            sample_rate; // Set by AudioDevice
  unsigned            sample_number; // Incremented by AudioDevice; only at end of each buffer
};
