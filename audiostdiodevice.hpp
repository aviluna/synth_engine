#pragma once

class AudioStdioDevice;

#include "audiodevice.hpp"

class AudioStdioDevice: public AudioDevice{
public:
  AudioStdioDevice();
  bool launch();
protected:
  bool sinkAction(AudioContext & context, Sample * buffer);
};
