#include "controlfilesource.hpp"

#include <fstream>

using namespace std;

ControlValue ControlFileSource::consume(){
 try_again:
  ControlValue read = default_value;
  if(file.eof() && repeat){
    file.clear(fstream::eofbit);
    file.seekg(0);
  }
  if(file.good()){
    file >> read;
    if(!file.good()) goto try_again;
  }
  return read;
}
