#include <iostream>

#include "audiogenerator.hpp"
#include "audiocontext.hpp"

bool AudioGenerator::connectOutput(AudioContext & context, AudioSink * where){
  if(!where->input_connected){
    where->input_connected = true;
    this->target = where;
    return true;
  }
  else{
    return false;
  }
}

void AudioGenerator::start(AudioContext & context){
  Sample * buffer = context.recycle_alloc();
  if(!buffer)
    buffer = new Sample[context.buffer_size];

  calculate(context, buffer);

  if(target)
    target->sink(context, buffer);
  else{
    std::cerr << "[BUG?] AudioGenerator not connected to anything!" << std::endl;
    context.recycle(buffer);
  }
}

void AudioGenerator::calculate(AudioContext & context, Sample * buffer){
  std::cerr << "[STUB] AudioGenerator::calculate default (no virtual override)" << std::endl;
  for(unsigned index = 0;
      index < context.buffer_size;
      index++){
    buffer[index] = 0.f;
  }
}
