#pragma once

class AudioGenerator;

#include "audiocontext.hpp"
#include "audiosink.hpp"

class AudioGenerator{
public:
  bool         connectOutput(AudioContext & context, AudioSink * where)
  // return true on success, false on error
    ;
  void         start(AudioContext & context)
  /*
   * starts the pipeline out of this generator
   */;

  AudioGenerator():
    target(0) {}
protected:
  virtual void calculate(AudioContext & context, Sample * buffer);

  AudioSink *  target;

};
