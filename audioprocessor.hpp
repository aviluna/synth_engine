#pragma once

class AudioProcessor;

#include "audiosink.hpp"
#include "sample.hpp"

class AudioProcessor: public AudioSink{
public:
  bool connectOutput(AudioContext & context, AudioSink * where)
  // return true on success, false on error
    ;
  bool sinkAction(AudioContext & context, Sample * buffer)
  /*
     because AudioProcessors are non-terminals, this will return false if it has
     successfully passed the buffer to another object, but will return true if it
     cannot pass the buffer to another object. This ensures the buffer gets freed.
     This function basically just calls process() on the buffer and passes it
     to the output.
  */
    ;
protected:
  virtual void process(AudioContext & context, Sample * buffer);
  AudioSink *  target;
};
