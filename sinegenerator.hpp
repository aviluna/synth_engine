#pragma once

class SineGenerator;

#include "audiogenerator.hpp"
#include "controlsource.hpp"

using namespace std;

class SineGenerator: public AudioGenerator{
  float phase;
  void calculate(AudioContext & context, Sample * buffer);
  ControlSource * frequency;
public:
  SineGenerator(ControlSource * p_frequency): phase(0.f), frequency(p_frequency) {}
};
