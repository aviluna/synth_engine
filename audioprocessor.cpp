#include <iostream>

#include "audioprocessor.hpp"

bool AudioProcessor::connectOutput(AudioContext & context, AudioSink * where){
  if(!where->input_connected){
    where->input_connected = true;
    this->target = where;
    return true;
  }
  else{
    return false;
  }
}

bool AudioProcessor::sinkAction(AudioContext & context, Sample * buffer){
  process(context, buffer);

  if(target){
    target->sink(context, buffer);
    return false;
  }
  else
    return true; // deallocate the buffer if we have nowhere to put it
}

void AudioProcessor::process(AudioContext & context, Sample * buffer){
  std::cerr << "[STUB] AudioProcessor::process default (no virtual override)" << std::endl;
  return; // null processor
}
