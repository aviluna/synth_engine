#include "audiocontext.hpp"
#include "controlsource.hpp"

ControlValue ControlSource::consume(){
  return +1.0;
}

void ControlSource::consume_n(ControlValue * values,
                              unsigned n){
  for(unsigned index = 0;
      index < n;
      index++){
    values[index] = consume(); // the naive default
  }
}
