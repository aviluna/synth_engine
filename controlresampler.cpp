#include <cmath>

#include "controlresampler.hpp"

template<class BinaryInterpolator>
ControlValue ControlResampler<BinaryInterpolator>::consume(){
  ControlValue return_value;
  while(accumulator >= 1.0){
    accumulator -= 1.0;
    last = next;
    next = src->consume();
  }
  return_value = binaryInterpolator.interpolate(last, next, accumulator);
  accumulator += rate_scaler;
  return return_value;
}

template<class BinaryInterpolator>
ControlResampler<BinaryInterpolator>::ControlResampler(
                                   unsigned p_src_rate,
                                   unsigned p_dst_rate,
                                   ControlSource * p_src){
  rate_scaler =
    static_cast<float>(p_src_rate) /
    static_cast<float>(p_dst_rate);
  src = p_src;

  last = src->consume();
  next = src->consume();

  accumulator = 0.0;
}

inline ControlValue
LinearInterpolator::interpolate(ControlValue a,
                                ControlValue b,
                                float t){
  return ((1.0f - t) * a) + (t * b);
}

template
ControlResampler<LinearInterpolator>::ControlResampler(unsigned, unsigned, ControlSource *);
