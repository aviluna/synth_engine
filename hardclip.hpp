#pragma once

class HardClip;

#include "audioprocessor.hpp"
#include "controlsource.hpp"

class HardClip: public AudioProcessor{
  ControlSource * gain;
  void process(AudioContext & context, Sample * buffer);
public:
  HardClip(ControlSource * p_gain):
    gain(p_gain){}
};
