#include <iostream>
#include "audiosink.hpp"

bool AudioSink::sinkAction(AudioContext & context, Sample * buffer){
  std::cerr << "[STUB] AudioSink::sinkAction default (no virtual override)" << std::endl;
  return true; // deallocate buffer
}

void AudioSink::sink(AudioContext & context, Sample * buffer){
  if(sinkAction(context, buffer)){
    // 'deallocate' by pushing the buffer onto the recycler
    context.recycle(buffer);
  }
  else{
    //don't deallocate
  }
}
