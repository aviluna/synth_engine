#pragma once

class ControlConstantSource;

#include "controlsource.hpp"

class ControlConstantSource: public ControlSource{
  const ControlValue constant;
public:
  ControlValue consume();
  ControlConstantSource(const ControlValue p_constant):
    constant(p_constant){}
};
