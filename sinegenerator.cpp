#define _USE_MATH_DEFINES
#include <cmath>

#include "sinegenerator.hpp"

const ControlValue TWO_PI = M_PI * 2.0;

void SineGenerator::calculate(AudioContext & context, Sample * buffer){
  for(unsigned index = 0;
      index < context.buffer_size;
      index++){
    phase +=
        (TWO_PI /
        static_cast<ControlValue>(context.sample_rate))
	      *
          frequency->consume();
    buffer[index] = sinf(phase);
	if(phase >= TWO_PI){
		phase -= TWO_PI;
	}
  }
}
