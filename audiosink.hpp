#pragma once

class AudioSink;

#include "audiocontext.hpp"
#include "sample.hpp"

class AudioSink{
public:
  void         sink(AudioContext & context, Sample * buffer);
  bool         input_connected; // whether the input has already been connected somewhere
  AudioSink():
    input_connected(false) {}
protected:
  virtual bool sinkAction(AudioContext & context, Sample * buffer);
  // used for actual things by derived classes
  // returns true if the AudioSink should keep the buffer
  // false if it should deallocate
};
