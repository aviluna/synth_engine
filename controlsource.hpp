#pragma once

typedef float ControlValue;
class ControlSource;

#include "audiocontext.hpp"

class ControlSource{
public:
  virtual ControlValue consume();
  virtual void consume_n(ControlValue * values, unsigned n);
};
