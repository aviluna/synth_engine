#include "hardclip.hpp"

using namespace std;

void HardClip::process(AudioContext & context, Sample * buffer){
  for(unsigned index = 0;
      index < context.buffer_size;
      index++){
    // XXX: not efficient. Needs non-branching replacement
    Sample temp = buffer[index] * gain->consume();
    if      (temp >= +1.0)
      temp = +1.0;
    else if (temp <= -1.0)
      temp = -1.0;
    buffer[index] = temp;
  }
}
