Aviluna Synth Engine
====================

## What is it?

So far it's a simple object hierarchy for audio synthesis and sample-exact (if you want) control.
The idea was to practice API design with virtual functions and derived classes. Also to write a synth.

Right now it can generate sine waves, hard-clip them, and change their frequency from data in a file.
The only output device so far is a simple proof of concept that runs at an 8KHz sampling rate and emits
16-bit little-endian samples on stdout.

A linear interpolating upsampler is implemented to allow ControlSources to be stretched over more samples.

## How's it work?

An AudioDevice is connected to a list of AudioGenerators. When the AudioDevice is launched, it endlessly
tells its AudioGenerators to start pipelines to its AudioSinks.

AudioGenerators allocate buffers and fill them with generated data.
AudioGenerators pass these buffers to AudioSinks.

An AudioSink can be an AudioProcessor, which can itself be connected to an AudioSink.
Before an AudioProcessor passes the buffer to the next AudioSink, it can modify the buffer,
by performing a lowpass filter on the data or perhaps reverb.

Finally an AudioDevice is also an AudioSink and this is where audio data terminates. Only one AudioGenerator
may be connected to the AudioSink of an AudioDevice, but in future an AudioMixer can be implemented to pre-mix
the data. This would not be hard to do.

Synthesis parameters, like the frequency of an oscillator, are controlled by ControlSources.

## What's implemented?

So far, two AudioGenerators are implemented:
+ AudioGenerator which produces a silent buffer
+ SineGenerator which produces a variable-pitch, full-amplitude sine wave

So far, two AudioProcessors are implemented:
+ AudioProcessor which passes the buffer through unmodified
+ HardClip which applies an amplitude gain to the incoming signal, but clamps the signal within the [-1,+1] range.

So far, three AudioSinks are implemented:
+ AudioSink which prints a stub message and cleanly recycles the buffer
+ AudioProcessor, as above
+ AudioDevice which is the terminal for all audible audio

So far, four ControlSources are implemented:
+ ControlSource which always provides +1.0.
+ ControlConstantSource which always provides the constant that it is initialised with
+ ControlFileSource which provides an optionally repeating (i.e. on EOF) stream of values from a file
  (note that this currently uses synchronous I/O)
+ ControlResampleSource which connects to another ControlSource, but upsamples (downsamples too, but not really)
its outputs with a linear interpolator so smooth sample-accurate parameter updates can occur from sparse data.
Currently this is not very efficient.

# License

Available under the terms of the GNU General Public License Version 3.0. See COPYING for details.

Copyright 2015 Aviluna Research

### Contact

If you really need to contact the author, please use GitLab's issue tracker.
