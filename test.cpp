#include "audiostdiodevice.hpp"
#include "sinegenerator.hpp"
#include "hardclip.hpp"
#include "controlconstantsource.hpp"
#include "controlresampler.hpp"
#include "controlfilesource.hpp"

int main(void){
  AudioStdioDevice      stdio_device;
  ControlFileSource     frequency_file("frequency.txt");
  ControlResampler
    <LinearInterpolator>interpolated_frequency(2, 8000, &frequency_file);
  SineGenerator         test_generator(&interpolated_frequency);
  ControlConstantSource clip_gain(1.5);
  HardClip              test_processor(&clip_gain);

  stdio_device.addGenerator(&test_generator);
  test_generator.connectOutput(stdio_device.context, &test_processor);
  test_processor.connectOutput(stdio_device.context, &stdio_device);

  while(stdio_device.launch());

  return 0;
}
