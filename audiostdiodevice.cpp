#include <cstdio>
#include <stdint.h>

#include "audiostdiodevice.hpp"

using namespace std;

AudioStdioDevice::AudioStdioDevice(){
  context.buffer_size = 4096;
  context.sample_rate = 8000;
  context.sample_number = 0;
}

bool AudioStdioDevice::launch(){
  if(feof(stdout)){
    context.recycler_invalidate();
    return false;
  }
  for(AudioGeneratorList::iterator iter = context.generator_list.begin();
      iter != context.generator_list.end();
      iter++){
    (*iter)->start(context);
  }
  return true;
}

bool AudioStdioDevice::sinkAction(AudioContext & context, Sample * buffer){
  int16_t * outbuf = new int16_t[context.buffer_size];
  for(unsigned index = 0;
      index < context.buffer_size;
      index++){
    outbuf[index] = buffer[index] * 32767.0;
  }
  fwrite(outbuf, sizeof(int16_t), context.buffer_size, stdout);
  delete[] outbuf;
  context.sample_number += context.buffer_size;
  return false; // deallocate buffer; we're done
}
