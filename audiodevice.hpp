#pragma once

class AudioDevice;

#include "sample.hpp"
#include "audiocontext.hpp"
#include "audiosink.hpp"
#include "audiogenerator.hpp"

class AudioDevice: public AudioSink{
public:
  AudioContext context;
  void addGenerator(AudioGenerator * generator);
  virtual bool launch() =0;
protected:
  bool sinkAction(AudioContext & context, Sample * buffer) =0;
};
