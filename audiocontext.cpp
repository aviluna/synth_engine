#include "audiocontext.hpp"

void AudioContext::recycle(Sample * buffer){
  buffer_recycler.push(buffer);
}

Sample * AudioContext::recycle_alloc(){
  if(buffer_recycler.empty()){
    return static_cast<Sample *>(0);
  }
  else{
    Sample * return_value = buffer_recycler.top();
    buffer_recycler.pop();
    return return_value;
  }
}

void AudioContext::recycler_invalidate(){
  while(!buffer_recycler.empty()){
    delete[] buffer_recycler.top();
    buffer_recycler.pop();
  }
}
