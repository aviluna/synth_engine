#pragma once

class ControlFileSource;

#include "controlsource.hpp"

#include <fstream>

class ControlFileSource: public ControlSource{
  std::fstream file;
  const ControlValue default_value;
  const bool repeat;
public:
  ControlFileSource(const char * p_filename,
                    const ControlValue p_default_value=0.0f,
                    const bool p_repeat=true):
    file(p_filename, std::fstream::in),
    default_value(p_default_value),
    repeat(p_repeat){}

  ControlValue consume();
};
