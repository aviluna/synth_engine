#pragma once

class LinearInterpolator;

template<class BinaryInterpolator>
class ControlResampler;

#include "controlsource.hpp"
#include "audiocontext.hpp"

template<class BinaryInterpolator>
class ControlResampler: public ControlSource{
  float rate_scaler; // input sample rate divided by destination samp
  float accumulator; // if >= 1 then get new sample
  Sample last, next;
  ControlSource * src;
  BinaryInterpolator binaryInterpolator;
public:
  ControlValue consume();

  ControlResampler<BinaryInterpolator>(unsigned p_src_rate,
                   unsigned p_dst_rate,
                   ControlSource * p_src);
};

struct LinearInterpolator{
  inline ControlValue interpolate(ControlValue a,
                                  ControlValue b,
                                  float t);
};
